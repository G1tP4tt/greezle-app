package de.hackathon_freiburg.sparks.greezthetreez;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import de.hackathon_freiburg.sparks.greezthetreez.services.Co2Service;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.LevelService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.ShopService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;

/**
 * Created by Patrice on 19.05.17.
 */


public class PagerAdapter extends FragmentStatePagerAdapter {

    private SavingsService savingsService;
    private UserService userService;
    private HistoryService historyService;
    int mNumOfTabs;
    private ShopService shopService;
    private LevelService levelService;
    private Co2Service co2Service;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, SavingsService savingsService,
                        UserService userService, HistoryService historyService,
                        ShopService shopService, LevelService levelService, Co2Service co2Service) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.savingsService = savingsService;
        this.userService = userService;
        this.historyService = historyService;
        this.shopService = shopService;
        this.levelService = levelService;
        this.co2Service = co2Service;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ProfileFragment tab1 = new ProfileFragment();
                tab1.setUserService(userService);
                tab1.setHistoryService(historyService);
                tab1.setLevelService(levelService);
                tab1.setCo2Service(co2Service);

                return tab1;
            case 1:
                DashboardFragment tab2 = new DashboardFragment();
                tab2.setSavingsService(savingsService);
                tab2.setUserService(userService);
                tab2.setHistoryService(historyService);
                return tab2;
            case 2:
                ShopFragment tab3 = new ShopFragment();
                tab3.setUserService(userService);
                tab3.setShopService(shopService);
                tab3.setSavingsService(savingsService);
                tab3.setHistoryService(historyService);
                return tab3;
            case 3:
                HistoryFragment tab4 = new HistoryFragment();
                tab4.setSavingsService(savingsService);
                tab4.setUserService(userService);
                tab4.setHistoryService(historyService);
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}