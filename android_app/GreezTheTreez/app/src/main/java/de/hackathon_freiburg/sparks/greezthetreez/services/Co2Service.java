package de.hackathon_freiburg.sparks.greezthetreez.services;

/**
 * Created by bene on 20.05.17.
 */

public interface Co2Service {
    public static final int NECESSARY_TREES = 2326;

    double currentProgressInPercent(long treeCount);

    long treesNecessaryForCo2Balance(long treeCount);
}
