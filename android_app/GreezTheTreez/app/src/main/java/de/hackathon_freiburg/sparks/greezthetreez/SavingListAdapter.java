package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.model.UserProfile;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;

public class SavingListAdapter extends RecyclerView.Adapter<SavingListAdapter.ItemViewHolder> {

    protected final LayoutInflater mInflater;
    protected final Context mContext;
    private final Fragment parentFragment;

    protected SavingsService savingsService;
    protected UserService userService;
    protected HistoryService historyService;

    public SavingListAdapter(LayoutInflater inflater, Context context, SavingsService savingsService,
                             UserService userService, Fragment parentFragment,
                             HistoryService historyService) {
        this.mInflater = inflater;
        this.mContext = context;
        this.savingsService = savingsService;
        this.userService = userService;
        this.parentFragment = parentFragment;
        this.historyService = historyService;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.list_item_savings, parent, false);
        return new ItemViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        List<Saving> savings = savingsService.getAllSavings();
        Saving saving = savings.get(position);

        holder.savingName.setText(saving.getTitle());
        holder.savingImage.setImageBitmap(Utilities.getBitmapFromAsset(mContext,
                saving.getIcon()));
        holder.savingGreezleValue.setText(String.valueOf(saving.getGreezlez()));


            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    SavingDialog savingDialog = new SavingDialog();
                    savingDialog.setSavingsService(savingsService);
                    savingDialog.setPosition(position);
                    savingDialog.setUserService(userService);
                    savingDialog.setHistoryService(historyService);
                    savingDialog.setTargetFragment(parentFragment,1);
                    savingDialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "dialog");
                }
            });
        




    }

    @Override
    public int getItemCount() {
        return savingsService.getAllSavings().size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView savingName;
        public ImageView savingImage;
        public TextView savingGreezleValue;
        public Button button;
        public TextView savingDescription;
        public TextView savingType ;
        public EditText greezleCode;

        public ItemViewHolder(View itemView) {
            super(itemView);
            savingName = (TextView) itemView.findViewById(R.id.savingName);
            savingImage = (ImageView) itemView.findViewById(R.id.savingImage);
            savingGreezleValue = (TextView) itemView.findViewById(R.id.savingGreezleValue);
            button = (Button) itemView.findViewById(R.id.btnOpenSaving);
            savingDescription = (TextView) itemView.findViewById(R.id.savingDialogDescription);
            savingType = (TextView) itemView.findViewById(R.id.greezleTypeText);
            greezleCode = (EditText) itemView.findViewById(R.id.greezleCodeIn);


        }
    }
}
