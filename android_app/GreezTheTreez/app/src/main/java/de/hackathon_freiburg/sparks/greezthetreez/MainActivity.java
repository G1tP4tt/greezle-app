package de.hackathon_freiburg.sparks.greezthetreez;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import de.hackathon_freiburg.sparks.greezthetreez.services.Co2Service;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.LevelService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.ShopService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.DefaultCo2Service;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.DefaultHistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.DefaultLevelService;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.DefaultSavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.DefaultShopService;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.SharedPreferencesHistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.impl.SharedPreferencesUserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;


public class MainActivity extends AppCompatActivity {



    private SavingsService savingsService;
    private UserService userService;
    private HistoryService historyService;
    private ShopService shopService;
    private LevelService levelService;
    private Co2Service co2Service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        savingsService = new DefaultSavingsService();
        userService = new SharedPreferencesUserService(this);

        historyService = new SharedPreferencesHistoryService(this);
        shopService = new DefaultShopService();

        levelService = new DefaultLevelService();

        co2Service = new DefaultCo2Service();

        TabLayout tabLayout = initializeTabLayout();

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(0);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(), savingsService, userService,
                        historyService, shopService, levelService, co2Service);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @NonNull
    private TabLayout initializeTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        Drawable person = getDrawable(R.drawable.ic_person_black_24dp);
        person.setTint(Utilities.getColor(this, R.color.white));

        Drawable dashBoard = getDrawable(R.drawable.ic_dashboard_black_24dp);
        dashBoard.setTint(Utilities.getColor(this, R.color.white));

        Drawable shoppingCard = getDrawable(R.drawable.ic_shopping_cart_black_24dp);
        shoppingCard.setTint(Utilities.getColor(this, R.color.white));

        Drawable bookMark = getDrawable(R.drawable.ic_bookmark_black_24dp);
        bookMark.setTint(Utilities.getColor(this, R.color.white));

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_profile)).setIcon(person));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_dashboard)).setIcon(dashBoard));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_shop)).setIcon(shoppingCard));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_history)).setIcon(bookMark));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        return tabLayout;
    }
}