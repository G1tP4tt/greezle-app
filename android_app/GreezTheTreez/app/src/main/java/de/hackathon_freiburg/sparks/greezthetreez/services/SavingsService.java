package de.hackathon_freiburg.sparks.greezthetreez.services;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;

/**
 * Created by bene on 19.05.17.
 */

public interface SavingsService {
    public List<Saving> getAllSavings();
}
