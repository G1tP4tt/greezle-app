package de.hackathon_freiburg.sparks.greezthetreez;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Purchase;
import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.model.UserProfile;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.ShopService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;


public class ShopDialog extends DialogFragment {

    final ShopDialog dialog = this;

    private int position;
    private UserService userService;

    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    private HistoryService historyService;
    private SavingListAdapter.ItemViewHolder itemViewHolder;

    public void setPosition(int position) {
        this.position = position;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void bindViewHolder(final SavingListAdapter.ItemViewHolder holder, final int position) {

        final List<Purchase> purchases = shopService.getAllPurchases();
        final Purchase purchase = purchases.get(position);

        holder.savingName.setText(purchase.getTitle());
        holder.savingImage.setImageBitmap(Utilities.getBitmapFromAsset(getContext(), purchase.getIcon()));
        holder.savingGreezleValue.setText(String.valueOf(purchase.getGreezlez()));
        holder.savingType.setVisibility(View.GONE);
        holder.greezleCode.setVisibility(View.GONE);
        holder.savingDescription.setText(purchase.getDescription());

        holder.button.setText(getString(R.string.btn_txt_plant));
        holder.button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userService.getCurrentGreezleBalance() + purchase.getGreezlez() >= 0) {
                    userService.addGreezles(purchase.getGreezlez());
                    UserProfile userProfile = userService.getUserProfile();
                    long trees = userProfile.getTreeCount() + 1;
                    userProfile.setTreeCount(trees);
                    userService.saveUserProfile(userProfile);
                    historyService.addEntry(purchase);
                    Toast.makeText(getContext(), R.string.purchase_confirm, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), R.string.purchase_deny, Toast.LENGTH_SHORT).show();
                }
                getDialog().dismiss();
            }
        });
    }

    public void setShopService(ShopService shopService) {
        this.shopService = shopService;
    }

    private ShopService shopService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.dialog_saving, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemViewHolder = new SavingListAdapter.
                ItemViewHolder(view.findViewById(R.id.savingGreezleLinear));
        bindViewHolder(itemViewHolder, position);
        activateCloseDialogListener(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setLayout(width, height);
    }

    public void activateCloseDialogListener(View view) {
        final FloatingActionButton closeSavingDialog = (FloatingActionButton) view.
                findViewById(R.id.btnCloseSavingDialog);

        closeSavingDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
