package de.hackathon_freiburg.sparks.greezthetreez.services;

public interface LevelService {
    int getLevel(long treeCount);

    long treesTillNextLevel(long treeCount);
}
