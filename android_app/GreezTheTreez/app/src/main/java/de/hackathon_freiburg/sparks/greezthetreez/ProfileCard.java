package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.hackathon_freiburg.sparks.greezthetreez.model.UserProfile;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;

public class ProfileCard {

    private UserService mUserService;
    private Context mContext;

    public ProfileCard(Context context, UserService userService) {
        this.mContext = context;
        this.mUserService = userService;
    }

    public void setProfileCard(View view) {
        CardView profileCard = (CardView) view.findViewById(R.id.profileCard);
        TextView profileName = (TextView) profileCard.findViewById(R.id.profileName);
        TextView greezleValue = (TextView) profileCard.findViewById(R.id.profileGreezleValue);
        TextView treesValue = (TextView) profileCard.findViewById(R.id.profileTreesValue);
        ImageView profilePic = (ImageView) profileCard.findViewById(R.id.profilePic);

        UserProfile userProfile = mUserService.getUserProfile();
        profileName.setText(userProfile.getName());
        greezleValue.setText(" " + String.valueOf(userProfile.getGreezleCount()));
        treesValue.setText(" " + String.valueOf(userProfile.getTreeCount()));
        profilePic.setImageBitmap(Utilities.getBitmapFromAsset(mContext,
                userProfile.getImage()));
    }
}
