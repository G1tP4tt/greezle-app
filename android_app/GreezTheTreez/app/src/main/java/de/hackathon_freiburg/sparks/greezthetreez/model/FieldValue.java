package de.hackathon_freiburg.sparks.greezthetreez.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bene on 20.05.17.
 */

public class FieldValue<T> {
    private SavingField<T> field;
    private T value;

    public FieldValue(SavingField<T> field, T value) {
        this.field = field;
        this.value = value;
    }

    public SavingField<T> getField() {
        return field;
    }

    public void setField(SavingField<T> field) {
        this.field = field;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
