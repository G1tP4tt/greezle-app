package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.R;
import de.hackathon_freiburg.sparks.greezthetreez.model.AbstractEntry;
import de.hackathon_freiburg.sparks.greezthetreez.model.FieldValue;
import de.hackathon_freiburg.sparks.greezthetreez.model.HistoryItem;
import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;

public class SharedPreferencesHistoryService implements HistoryService {

    private static final Type HISTORY_LIST_TYPE = new TypeToken<List<HistoryItem>>() {
    }.getType();

    private final Context context;

    public SharedPreferencesHistoryService(final Context context) {
        this.context = context;
    }

    @Override
    public List<HistoryItem> getHistory() {
        // 1. get history json from shared preferences
        final String historyJson = getSharedPreferences().getString(context.getString(R.string.history_key), "");

        // 2. parse json to List
        List<HistoryItem> historyItems = null;
        if (historyJson != null && historyJson.length() >= 2 && !historyJson.equals("null")) {
            historyItems = new Gson().fromJson(historyJson, HISTORY_LIST_TYPE);
        }

        if (historyItems == null) {
            historyItems = new ArrayList<>();
        }

        return Collections.unmodifiableList(historyItems);
    }

    @Override
    public HistoryItem addEntry(final AbstractEntry entry) {
        final HistoryItem historyItem = new HistoryItem(entry);
        addHistoryItem(historyItem);
        return historyItem;
    }

    @Override
    public HistoryItem addSavingWithFields(Saving saving, List<FieldValue> fieldValues) {
        final HistoryItem historyItem = new HistoryItem(saving, fieldValues);
        addHistoryItem(historyItem);
        return historyItem;
    }

    private void addHistoryItem(HistoryItem historyItem) {
        final List<HistoryItem> historyItems = new ArrayList<>(getHistory());
        historyItems.add(historyItem);
        saveHistory(historyItems);
    }

    protected void saveHistory(final List<HistoryItem> history) {
        // 1. parse history to json
        final String historyListJson = new Gson().toJson(history, HISTORY_LIST_TYPE);

        // 2. save json to shared preferences
        // save user profile json
        final SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(context.getString(R.string.history_key), historyListJson);
        editor.commit();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public HistoryItem addEntry(AbstractEntry entry, int greezelz){
        final HistoryItem historyItem = new HistoryItem(entry);
        historyItem.setGreezelz(greezelz);
        addHistoryItem(historyItem);
        return historyItem;
    }

    public boolean isSavingTimeLocked(Saving saving) {
        final List<HistoryItem> historyItems = new ArrayList<>(getHistory());
        Collections.sort(historyItems, Collections.reverseOrder());
        HistoryItem recentHistoryItem = null;
        for (HistoryItem historyItem : historyItems) {
            if (historyItem.getEntry().getTitle().equals(saving.getTitle())) {
                recentHistoryItem = historyItem;
                break;
            }
        }

        if (recentHistoryItem != null) {
            Saving recentSaving = null;
            for (Saving savingType : new DefaultSavingsService().getAllSavings()) {
                if (savingType.getTitle().equals(recentHistoryItem.getEntry().getTitle())) {
                    recentSaving = savingType;
                    break;
                }
            }
            if (recentSaving != null) {
                long lockDurationOfRecentHistoryItem = recentSaving.getLockDuration();
                return new Date().getTime() - recentHistoryItem.getTimestamp().getTime() < lockDurationOfRecentHistoryItem;
            }
        }
        return false;
    }
}
