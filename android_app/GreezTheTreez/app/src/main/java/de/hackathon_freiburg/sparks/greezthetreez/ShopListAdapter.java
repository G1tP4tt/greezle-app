package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Purchase;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.ShopService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ItemViewHolder> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final Fragment parentFragment;

    private ShopService shopService;
    protected SavingsService savingsService;
    protected UserService userService;
    protected HistoryService historyService;


    public ShopListAdapter(LayoutInflater inflater, Context context, ShopService shopService,
                           Fragment parentFragment, SavingsService savingsService,
                           UserService userService, HistoryService historyService) {
        this.mInflater = inflater;
        this.mContext = context;
        this.shopService = shopService;
        this.parentFragment = parentFragment;
        this.savingsService = savingsService;
        this.userService = userService;
        this.historyService = historyService;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.list_item_purchase, parent, false);
        return new ShopListAdapter.ItemViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ShopListAdapter.ItemViewHolder holder, final int position) {

        List<Purchase> purchases = shopService.getAllPurchases();
        Purchase purchase = purchases.get(position);

        holder.purchaseName.setText(purchase.getTitle());
        holder.purchaseImage.setImageBitmap(Utilities.getBitmapFromAsset(mContext,
                purchase.getIcon()));
        holder.purchaseGreezleValue.setText(String.valueOf(purchase.getGreezlez()));

        if(purchase.getGreezlez() == 0){
            holder.purchaseUnit.setText("€");
            holder.purchaseGreezleValue.setText("1");

        }else{
            holder.purchaseGreezleValue.setText(String.valueOf(purchase.getGreezlez()));
        }
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShopDialog shopDialog = new ShopDialog();
                shopDialog.setShopService(shopService);
                shopDialog.setPosition(position);
                shopDialog.setUserService(userService);
                shopDialog.setHistoryService(historyService);
                shopDialog.setTargetFragment(parentFragment,1);
                shopDialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "dialog");
            }
        });
    }



    @Override
    public int getItemCount() {
        return shopService.getAllPurchases().size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView purchaseName;
        public ImageView purchaseImage;
        public TextView purchaseGreezleValue;
        public Button button;
        public TextView purchaseUnit ;

        public ItemViewHolder(View itemView) {
            super(itemView);
            purchaseName = (TextView) itemView.findViewById(R.id.purchaseName);
            purchaseImage = (ImageView) itemView.findViewById(R.id.purchaseImage);
            purchaseGreezleValue = (TextView) itemView.findViewById(R.id.purchaseGreezleValue);
            button = (Button) itemView.findViewById(R.id.btnOpenSaving);

            purchaseUnit = (TextView) itemView.findViewById(R.id.purchaseGreezle);

        }
    }
}
