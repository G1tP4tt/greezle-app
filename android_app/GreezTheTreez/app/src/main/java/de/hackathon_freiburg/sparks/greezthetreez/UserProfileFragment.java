package de.hackathon_freiburg.sparks.greezthetreez;

import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;

interface UserProfileFragment {
    void setUserService(UserService userService);
}
