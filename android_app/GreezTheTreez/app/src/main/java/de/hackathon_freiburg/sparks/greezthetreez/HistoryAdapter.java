package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.HistoryItem;
import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ItemViewHolder> {

    private final LayoutInflater mInflater;
    private final Context mContext;

    private HistoryService historyService;

    public HistoryAdapter(LayoutInflater inflater, Context context, HistoryService historyService) {
        this.mInflater = inflater;
        this.mContext = context;
        this.historyService = historyService;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.list_item_history, parent, false);
        return new ItemViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        List<HistoryItem> historyItems = new ArrayList<>(historyService.getHistory());
        Collections.sort(historyItems, Collections.reverseOrder());
        HistoryItem historyItem = historyItems.get(position);

        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        holder.historyTime.setText(dt.format(historyItem.getTimestamp()));
        holder.historyName.setText(historyItem.getEntry().getTitle());
        holder.historyPoints.setText(String.valueOf(historyItem.getGreezlez()));
        holder.itemView.invalidate();
    }

    @Override
    public int getItemCount() {
        return historyService.getHistory().size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView historyTime;
        public TextView historyName;
        public TextView historyPoints;

        public ItemViewHolder(View itemView) {
            super(itemView);
            historyTime = (TextView) itemView.findViewById(R.id.historyTime);
            historyName = (TextView) itemView.findViewById(R.id.historyName);
            historyPoints = (TextView) itemView.findViewById(R.id.historyPoints);
        }
    }
}
