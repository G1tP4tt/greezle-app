package de.hackathon_freiburg.sparks.greezthetreez.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by bene on 19.05.17.
 */

public class UserProfile {
    @Expose
    private String name;
    @Expose
    private String image;
    @Expose
    private int level;
    @Expose
    private long greezleCount;
    @Expose
    private long treeCount;
    @Expose(serialize = false, deserialize = false)
    private List<HistoryItem> userHistory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getGreezleCount() {
        return greezleCount;
    }

    public void setGreezleCount(long greezleCount) {
        this.greezleCount = greezleCount;
    }

    public long getTreeCount() {
        return treeCount;
    }

    public void setTreeCount(long treeCount) {
        this.treeCount = treeCount;
    }

    public List<HistoryItem> getUserHistory() {
        return userHistory;
    }

    public void setUserHistory(List<HistoryItem> userHistory) {
        this.userHistory = userHistory;
    }
}
