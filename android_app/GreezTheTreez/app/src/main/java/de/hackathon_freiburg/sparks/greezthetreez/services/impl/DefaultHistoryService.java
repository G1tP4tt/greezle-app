package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.AbstractEntry;
import de.hackathon_freiburg.sparks.greezthetreez.model.FieldValue;
import de.hackathon_freiburg.sparks.greezthetreez.model.HistoryItem;
import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;

public class DefaultHistoryService implements HistoryService {
    @Override
    public HistoryItem addEntry(AbstractEntry entry, int greezelz) {
        return null;
    }

    List<HistoryItem> items = new ArrayList<>();

    @Override
    public List<HistoryItem> getHistory() {
        return items;
    }

    @Override
    public HistoryItem addEntry(AbstractEntry entry) {
        HistoryItem temp = new HistoryItem();
        temp.setEntry(entry);
        temp.setTimestamp(new Date());
        items.add(temp);
        return temp;
    }

    @Override
    public HistoryItem addSavingWithFields(Saving saving, List<FieldValue> fieldValues) {
        return addEntry(saving);
    }

    @Override
    public boolean isSavingTimeLocked(Saving saving) {
        return false;
    }

    //returns true if Item has no lockDuration or  lockDuration has passed
    public boolean isSavingTimeLocked(HistoryItem item) {
        int index = getHistory().lastIndexOf(item.getEntry());
        if (index >= 0) {
            HistoryItem recentHistoryItem = getHistory().get(index);
            if (recentHistoryItem.getEntry().getClass() == Saving.class) {
                long lockDurationOfRecentHistoryItem = ((Saving) recentHistoryItem.getEntry()).getLockDuration();
                return item.getTimestamp().getTime() - recentHistoryItem.getTimestamp().getTime() > lockDurationOfRecentHistoryItem;
            }
        }
        return true;
    }
}
