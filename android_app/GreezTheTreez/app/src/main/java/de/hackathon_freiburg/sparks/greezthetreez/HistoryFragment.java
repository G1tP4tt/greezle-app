package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;

public class HistoryFragment extends Fragment implements UserProfileFragment {

    private UserService userService;
    private HistoryService historyService;
    private SavingsService savingsService;


    public void setSavingsService(SavingsService savingsService) {
        this.savingsService = savingsService;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);

        RecyclerView historyListView = (RecyclerView) rootView.findViewById(R.id.listview_history);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        historyListView.setLayoutManager(linearLayoutManager);
        HistoryAdapter savingArrayAdapter = new HistoryAdapter(inflater, getContext(),
                historyService);
        historyListView.setAdapter(savingArrayAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProfileCard profileCard = new ProfileCard(getContext(), userService);
        profileCard.setProfileCard(view);
    }

    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }



    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}