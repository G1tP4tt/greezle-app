package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;

public class SavingDialogListAdapter extends SavingListAdapter {

    public SavingDialogListAdapter(LayoutInflater inflater, Context context, SavingsService savingsService, UserService userService, Fragment parentFragment, HistoryService historyService) {
        super(inflater, context, savingsService, userService, parentFragment, historyService);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.dialog_saving, parent, false);
        return new ItemViewHolder(rootView);
    }

    public void onBindViewHolder(ItemViewHolder holder, int position) {
        super.onBindViewHolder(holder,position);

        List<Saving> savings = savingsService.getAllSavings();
        Saving saving = savings.get(position);
    }

    public class ItemViewHolder extends SavingListAdapter.ItemViewHolder {

        public ItemViewHolder(View itemView) {
            super(itemView);
        }
    }
}
