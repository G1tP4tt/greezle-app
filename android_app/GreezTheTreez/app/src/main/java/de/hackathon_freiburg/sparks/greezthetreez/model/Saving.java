package de.hackathon_freiburg.sparks.greezthetreez.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by bene on 19.05.17.
 */

public class Saving extends AbstractEntry {
    private long lockDuration;
    private List<SavingField> savingFields;

    public String getSavingType() {
        return savingType;
    }

    public void setSavingType(String savingType) {
        this.savingType = savingType;
    }

    private String savingType;

    public long getLockDuration() {
        return lockDuration;
    }

    public void setLockDuration(long lockDuration) {
        this.lockDuration = lockDuration;
    }

    public List<SavingField> getSavingFields() {
        return savingFields;
    }

    public void setSavingFields(List<SavingField> savingFields) {
        this.savingFields = savingFields;
    }
}
