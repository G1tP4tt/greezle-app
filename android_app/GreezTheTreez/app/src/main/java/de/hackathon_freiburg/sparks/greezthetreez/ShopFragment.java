package de.hackathon_freiburg.sparks.greezthetreez;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.ShopService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;

public class ShopFragment extends Fragment implements UserProfileFragment{

    private UserService userService;
    private ShopService shopService;

    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    private HistoryService historyService;

    public void setSavingsService(SavingsService savingsService) {
        this.savingsService = savingsService;
    }

    private SavingsService savingsService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    public void setShopService(ShopService shopService) {
        this.shopService = shopService;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_dashboard, container, false);

        RecyclerView purchaseListView = (RecyclerView) rootview.findViewById(R.id.listview_savings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        purchaseListView.setLayoutManager(linearLayoutManager);
        ShopListAdapter purchaseArrayAdaper = new ShopListAdapter(inflater, getContext(),
                shopService, this, savingsService, userService, historyService);
        purchaseListView.setAdapter(purchaseArrayAdaper);

        return rootview;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ProfileCard profileCard = new ProfileCard(getContext(), userService);
        profileCard.setProfileCard(view);
    }
}
