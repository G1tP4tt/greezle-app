package de.hackathon_freiburg.sparks.greezthetreez.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HistoryItem implements Comparable {
    private AbstractEntry entry;
    private Date timestamp;
    private List<FieldValue> fieldValues;
    private long greezelAmount;

    public HistoryItem() {

    }

    public HistoryItem(AbstractEntry entry) {
        this(entry, new ArrayList<FieldValue>());

    }

    public HistoryItem(AbstractEntry entry, List<FieldValue> fieldValues) {
        super();
        this.entry = entry;
        this.fieldValues = fieldValues;
        this.timestamp = new Date();

        this.greezelAmount = calculateGreezels(entry, fieldValues);
    }

    private long calculateGreezels(AbstractEntry entry, List<FieldValue> fieldValues) {
        final long baseGreezlez = entry.getGreezlez();

        final List<FieldValue> multiplyFields = new ArrayList<>();
        for (FieldValue fieldValue : fieldValues) {
            if (fieldValue.getField().isMultiply()) {
                multiplyFields.add(fieldValue);
            }
        }

        long calculatedGreezelz = baseGreezlez;
        for (FieldValue multiplyField : multiplyFields) {
            try {
                calculatedGreezelz = calculatedGreezelz * Long.valueOf(multiplyField.getValue().toString());
            } catch (Exception e) {
                // do nuffin
            }
        }

        return calculatedGreezelz;
    }

    public long getGreezlez() {
        return greezelAmount;
    }

    public void setGreezelz(long greezelz) {
        this.greezelAmount = greezelz;
    }

    public AbstractEntry getEntry() {
        return entry;
    }

    public void setEntry(AbstractEntry entry) {
        this.entry = entry;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if (o != null && o instanceof HistoryItem) {
            final HistoryItem other = (HistoryItem) o;
            return this.timestamp.compareTo(other.timestamp);
        }
        return -1;
    }
}
