package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import de.hackathon_freiburg.sparks.greezthetreez.R;
import de.hackathon_freiburg.sparks.greezthetreez.model.UserProfile;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;

public class SharedPreferencesUserService implements UserService {

    private final Context context;

    public SharedPreferencesUserService(final Context context) {
        this.context = context;
    }

    @Override
    public UserProfile getUserProfile() {
        // 1. get user profile json from shared preferences
        final String userProfileJson = getSharedPreferences().getString(context.getString(R.string.user_profile_key), "");

        // 2. parse json
        final UserProfile userProfile;
        if (userProfileJson != null && userProfileJson.length() >= 2) {
            userProfile = new Gson().fromJson(userProfileJson, UserProfile.class);
        } else {
            userProfile = initDefaultUserProfile();
        }

        // 3. get history and add it to user profile
        userProfile.setUserHistory(new SharedPreferencesHistoryService(context).getHistory());

        return userProfile;
    }

    private UserProfile initDefaultUserProfile() {
        // create default user
        final UserProfile userProfile = new UserProfile();
        userProfile.setTreeCount(0);
        userProfile.setGreezleCount(0);
        userProfile.setImage("boaty.jpg");
        userProfile.setName(context.getString(R.string.user_default_name));

        // save default user
        saveUserProfile(userProfile);

        return userProfile;
    }

    @Override
    public void saveUserProfile(final UserProfile userProfile) {
        // parse user profile object to json
        final String userProfileJson = new Gson().toJson(userProfile, UserProfile.class);

        // save user profile json
        final SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(context.getString(R.string.user_profile_key), userProfileJson);
        editor.commit();

        // save history
        new SharedPreferencesHistoryService(context).saveHistory(userProfile.getUserHistory());
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Override
    public void addGreezles(int newGreezels) {
        final UserProfile userProfile = getUserProfile();
        if(getUserProfile().getGreezleCount() + newGreezels < 0 ) {
            throw new RuntimeException("User has not enough greezlez!");
        } else {
            userProfile.setGreezleCount(userProfile.getGreezleCount() + newGreezels);
        }
        saveUserProfile(userProfile);
    }

    @Override
    public long getCurrentGreezleBalance() {
        return getUserProfile().getGreezleCount();
    }
}
