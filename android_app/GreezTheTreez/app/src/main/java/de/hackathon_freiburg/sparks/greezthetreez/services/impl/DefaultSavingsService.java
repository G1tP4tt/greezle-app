package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;

/**
 * Created by bene on 19.05.17.
 */

public class DefaultSavingsService implements SavingsService {

    private static final long ONE_MINUTE_IN_MS = 60000;

    @Override
    public List<Saving> getAllSavings() {
        final List<Saving> savings = new ArrayList<>();

        final Saving saving1 = new Saving();
        saving1.setLockDuration(ONE_MINUTE_IN_MS);
        saving1.setTitle("Mit dem Rad zur Arbeit.");
        saving1.setGreezlez(1);
        saving1.setDescription("Fahre mit dem Fahrrad statt mit dem Auto und erhalte "+saving1.getGreezlez()+" Greezlez pro Kilometer. (Alle "+saving1.getLockDuration()/60000/60+" Stunden einsetzbar)");
        saving1.setIcon("bicycle.jpg");
        saving1.setSavingType("Distanz in Kilometern");
        savings.add(saving1);

        final Saving changeBulb = new Saving();
        changeBulb.setLockDuration(ONE_MINUTE_IN_MS*30);
        changeBulb.setTitle("Alte Glühbirne durch sparsamere ersetzen.");
        changeBulb.setDescription("Der CO2-Ausstoß einer herkömlichen Glühbirne beträgt das 5,4-fache einer Energiesparlampe.");
        changeBulb.setGreezlez(54);
        changeBulb.setIcon("bulb.jpg");
        changeBulb.setSavingType("Anzahl");
        savings.add(changeBulb);

        final Saving solar = new Saving();
        solar.setLockDuration(ONE_MINUTE_IN_MS*60*24*360); //Once a year
        solar.setTitle("Solaranlage installieren.");
        solar.setDescription("Eine Solaranlage auf dem eigenen Dach ist eine tolle Investion.");
        solar.setGreezlez(1500);
        solar.setIcon("Solar.jpg");
        solar.setSavingType("Greezle Code");
        savings.add(solar);

        final Saving grueneFlotte = new Saving();
        grueneFlotte.setLockDuration(ONE_MINUTE_IN_MS*60*24*30); //Once a year
        grueneFlotte.setTitle("Registrierung bei \"Grüne Flotte\"");
        grueneFlotte.setDescription("Die grüne Flotte ist eine umweltfreundliche und flexible Alternative zum eigenen Auto.");
        grueneFlotte.setGreezlez(800);
        grueneFlotte.setIcon("GrueneFlotte.jpg");
        grueneFlotte.setSavingType("Greezle Code");
        savings.add(grueneFlotte);

        final Saving regioKarte = new Saving();
        regioKarte.setLockDuration(ONE_MINUTE_IN_MS*60*24*30); //Once a year
        regioKarte.setTitle("Kauf einer Regiokarte");
        regioKarte.setDescription("Mit der Regiokarte werden Umwelt und Geldbeutel geschont.");
        regioKarte.setGreezlez(100);
        regioKarte.setIcon("Regiokarte.jpg");
        grueneFlotte.setSavingType("Greezle Code");
        savings.add(regioKarte);

        final Saving registrationForEbikes = new Saving();
        registrationForEbikes.setTitle("Mitgliedschaft bei Freiburgs E-Bikes Abschließen.");
        registrationForEbikes.setLockDuration(ONE_MINUTE_IN_MS);
        registrationForEbikes.setDescription("E-Bikes sind cool, Mkay.");
        registrationForEbikes.setGreezlez(150);
        registrationForEbikes.setIcon("e_bike.jpg");
        grueneFlotte.setSavingType("Greezle Code");
        savings.add(registrationForEbikes);

        return Collections.unmodifiableList(savings);
    }
}
