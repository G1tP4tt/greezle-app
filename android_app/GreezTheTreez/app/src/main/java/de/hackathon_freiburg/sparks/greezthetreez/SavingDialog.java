package de.hackathon_freiburg.sparks.greezthetreez;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import de.hackathon_freiburg.sparks.greezthetreez.util.Utilities;


public class SavingDialog extends DialogFragment {

    final SavingDialog dialog = this;

    private int position;
    private UserService userService;

    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    private HistoryService historyService;
    private SavingListAdapter.ItemViewHolder itemViewHolder;

    public void setPosition(int position) {
        this.position = position;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void bindViewHolder(final SavingListAdapter.ItemViewHolder holder, final int position) {

        List<Saving> savings = savingsService.getAllSavings();
        final Saving saving = savings.get(position);

        holder.savingName.setText(saving.getTitle());
        holder.savingImage.setImageBitmap(Utilities.getBitmapFromAsset(getContext(),
                saving.getIcon()));

        holder.savingType.setText(saving.getSavingType());
        holder.savingGreezleValue.setText(String.valueOf(saving.getGreezlez()));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!historyService.isSavingTimeLocked(saving)) {

                    int greezlesGained ;
                    if (!saving.getSavingType().equals("Greezle Code")){
                        greezlesGained = saving.getGreezlez() * Integer.parseInt(
                                holder.greezleCode.getText().toString());
                    }else{
                        greezlesGained = saving.getGreezlez();
                    }
                    userService.addGreezles(greezlesGained);
                    historyService.addEntry(saving, greezlesGained);


                    getTargetFragment().onActivityResult(1, 1, null);
                    Toast.makeText(getContext(), R.string.greezles_confirm, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(getContext(), R.string.greezles_deny, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }
        });
        holder.savingDescription.setText(saving.getDescription());
    }

    public void setSavingsService(SavingsService savingsService) {
        this.savingsService = savingsService;
    }

    private SavingsService savingsService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.dialog_saving, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemViewHolder = new SavingListAdapter.
                ItemViewHolder(view.findViewById(R.id.savingGreezleLinear));
        bindViewHolder(itemViewHolder, position);



        activateCloseDialogListener(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setLayout(width, height);
    }

    public void activateCloseDialogListener(View view){
        final FloatingActionButton closeSavingDialog = (FloatingActionButton) view.
                findViewById(R.id.btnCloseSavingDialog);

        closeSavingDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
