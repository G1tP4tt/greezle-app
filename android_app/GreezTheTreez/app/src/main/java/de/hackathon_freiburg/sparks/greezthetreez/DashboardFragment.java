package de.hackathon_freiburg.sparks.greezthetreez;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import de.hackathon_freiburg.sparks.greezthetreez.model.UserProfile;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.SavingsService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;


public class DashboardFragment extends Fragment implements UserProfileFragment {

    private Handler progressBarHandler = new Handler();

    private SavingsService savingsService;
    private UserService userService;
    private HistoryService historyService;


    private SavingListAdapter savingArrayAdapter;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    public void setSavingsService(SavingsService savingsService) {
        this.savingsService = savingsService;
    }
    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_dashboard, container, false);

        RecyclerView savingListView = (RecyclerView) rootview.findViewById(R.id.listview_savings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        savingListView.setLayoutManager(linearLayoutManager);
        savingArrayAdapter = new SavingListAdapter(inflater, getContext(),
                savingsService, userService, this, historyService);
        savingListView.setAdapter(savingArrayAdapter);
        savingArrayAdapter.notifyDataSetChanged();



        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ProfileCard profileCard = new ProfileCard(getContext(), userService);
        profileCard.setProfileCard(view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        savingArrayAdapter.notifyDataSetChanged();

        progressBarHandler.post(new Runnable() {
            public void run() {
                TextView profileGreezleValue = (TextView) getActivity().
                        findViewById(R.id.profileGreezleValue);
                UserProfile userProfile = userService.getUserProfile();
                profileGreezleValue.setText(String.valueOf(userProfile.getGreezleCount()));
                profileGreezleValue.invalidate();
                profileGreezleValue.forceLayout();
            }
        });
    }

}