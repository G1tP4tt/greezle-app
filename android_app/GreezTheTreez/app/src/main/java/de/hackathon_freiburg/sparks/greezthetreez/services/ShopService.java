package de.hackathon_freiburg.sparks.greezthetreez.services;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Purchase;

/**
 * Created by Alex on 19.05.2017.
 */

public interface ShopService {
    public List<Purchase> getAllPurchases();

}
