package de.hackathon_freiburg.sparks.greezthetreez.model;

import java.math.BigDecimal;

/**
 * Created by bene on 19.05.17.
 */

public class Purchase extends AbstractEntry {
    private int quantityOfTrees;
    private boolean inAppPayment;
    private BigDecimal realMoneyCost;

    public int getQuantityOfTrees() {
        return quantityOfTrees;
    }

    public void setQuantityOfTrees(int quantityOfTrees) {
        this.quantityOfTrees = quantityOfTrees;
    }

    public boolean isInAppPayment() {
        return inAppPayment;
    }

    public void setInAppPayment(boolean inAppPayment) {
        this.inAppPayment = inAppPayment;
    }

    public BigDecimal getRealMoneyCost() {
        return realMoneyCost;
    }

    public void setRealMoneyCost(BigDecimal realMoneyCost) {
        this.realMoneyCost = realMoneyCost;
    }
}
