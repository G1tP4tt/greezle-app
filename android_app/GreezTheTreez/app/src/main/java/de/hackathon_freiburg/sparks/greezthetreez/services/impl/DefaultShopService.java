package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.Purchase;
import de.hackathon_freiburg.sparks.greezthetreez.services.ShopService;

/**
 * Created by User on 19.05.2017.
 */

public class DefaultShopService implements ShopService{

    @Override
    public List<Purchase> getAllPurchases() {
        final List<Purchase> purchases = new ArrayList<>();
        Purchase p1 = new Purchase();
        p1.setTitle("Kleine Pflanze. Große Wirkung.");
        p1.setIcon("BaumPflanzen.jpg");
        p1.setDescription("Nutze Deine Greezels und pflanze einen Baum.");
        p1.setQuantityOfTrees(1);
        p1.setGreezlez(-300);
        p1.setInAppPayment(true);
        p1.setRealMoneyCost(new BigDecimal("150"));
        purchases.add(p1);

        Purchase plantForTheP = new Purchase();
        plantForTheP.setTitle("Ein Baum für einen Euro.");
        plantForTheP.setIcon("PlantForThePlanet.jpg");
        plantForTheP.setDescription("Spende an Plant for the Planet. \nBankverbindung:\n" +
                "Sozialbank, München\n" +
                "IBAN: DE13 7002 0500 0000 200 000\n" +
                "BIC/SWIFT: BFSWDE33MUE");
        plantForTheP.setQuantityOfTrees(1);
        plantForTheP.setGreezlez(0);
        plantForTheP.setInAppPayment(true);
        plantForTheP.setRealMoneyCost(new BigDecimal("150"));
        purchases.add(plantForTheP);

        return purchases;
    }
}
