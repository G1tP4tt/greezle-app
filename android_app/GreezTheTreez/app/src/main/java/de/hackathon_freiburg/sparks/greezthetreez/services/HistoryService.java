package de.hackathon_freiburg.sparks.greezthetreez.services;

import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.AbstractEntry;
import de.hackathon_freiburg.sparks.greezthetreez.model.FieldValue;
import de.hackathon_freiburg.sparks.greezthetreez.model.HistoryItem;
import de.hackathon_freiburg.sparks.greezthetreez.model.Saving;

public interface HistoryService {
    List<HistoryItem> getHistory();

    HistoryItem addEntry(AbstractEntry entry);

    HistoryItem addEntry(AbstractEntry entry, int greezelz);

    HistoryItem addSavingWithFields(Saving saving, List<FieldValue> fieldValues);


    boolean isSavingTimeLocked(Saving saving);
}
