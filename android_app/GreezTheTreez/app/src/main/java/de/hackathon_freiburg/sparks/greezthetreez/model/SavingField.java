package de.hackathon_freiburg.sparks.greezthetreez.model;

import java.lang.reflect.Type;

/**
 * Created by bene on 20.05.17.
 */

public class SavingField<T> {
    private Class<T> type;
    private String name;
    private boolean multiply;

    public Class<T> getType() {
        return type;
    }

    public void setType(Class<T> type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMultiply() {
        return multiply;
    }

    public void setMultiply(boolean multiply) {
        this.multiply = multiply;
    }
}
