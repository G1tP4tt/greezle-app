package de.hackathon_freiburg.sparks.greezthetreez.model;

import java.util.Objects;

public class AbstractEntry {
    private int greezlez;
    private String title;
    private String description;
    private String icon;

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()){ return false;}
        return ((AbstractEntry) o).title.equals(this.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    public int getGreezlez() {
        return greezlez;
    }

    public void setGreezlez(int greezlez) {
        this.greezlez = greezlez;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
