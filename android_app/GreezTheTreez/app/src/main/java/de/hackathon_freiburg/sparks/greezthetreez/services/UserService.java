package de.hackathon_freiburg.sparks.greezthetreez.services;

import de.hackathon_freiburg.sparks.greezthetreez.model.UserProfile;

/**
 * Created by bene on 19.05.17.
 */

public interface UserService {
    public UserProfile getUserProfile();

    public void saveUserProfile(UserProfile userProfile);

    public void addGreezles(int newGreezels);

    public long getCurrentGreezleBalance();
}
