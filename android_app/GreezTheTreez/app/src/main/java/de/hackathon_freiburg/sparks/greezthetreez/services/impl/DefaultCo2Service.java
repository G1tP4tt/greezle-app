package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import de.hackathon_freiburg.sparks.greezthetreez.services.Co2Service;

public class DefaultCo2Service implements Co2Service {
    @Override
    public double currentProgressInPercent(long treeCount) {
        return ((double) treeCount) / ((double) DefaultCo2Service.NECESSARY_TREES);
    }

    @Override
    public long treesNecessaryForCo2Balance(long treeCount) {
        return DefaultCo2Service.NECESSARY_TREES - treeCount;
    }
}
