package de.hackathon_freiburg.sparks.greezthetreez;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hackathon_freiburg.sparks.greezthetreez.model.HistoryItem;
import de.hackathon_freiburg.sparks.greezthetreez.services.Co2Service;
import de.hackathon_freiburg.sparks.greezthetreez.services.HistoryService;
import de.hackathon_freiburg.sparks.greezthetreez.services.LevelService;
import de.hackathon_freiburg.sparks.greezthetreez.services.UserService;
import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class ProfileFragment extends Fragment {

    private Handler progressBarHandler = new Handler();

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    private UserService userService;
    private HistoryService historyService;

    public void setLevelService(LevelService levelService) {
        this.levelService = levelService;
    }

    private LevelService levelService;

    public void setCo2Service(Co2Service co2Service) {
        this.co2Service = co2Service;
    }

    private Co2Service co2Service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        LineChartView greezle_chart = (LineChartView) rootView.findViewById(R.id.greezle_chart);
        // LineChartView tree_chart = (LineChartView) rootView.findViewById(R.id.tree_chart);

        List<HistoryItem> historyItems = historyService.getHistory();
        List<PointValue> values = new ArrayList<PointValue>();
        float result = 0;
        float i = 0;
        for (HistoryItem historyItem : historyItems) {

            float x = i++;
            result = result + historyItem.getGreezlez();
            float y = result;
            values.add(new PointValue(x, y));

        }
        Line line = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            line = new Line(values).setColor(getActivity().
                    getColor(R.color.colorPrimaryDark));
        }
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);
        LineChartData data = new LineChartData();
        data.setLines(lines);
        greezle_chart.setLineChartData(data);
        greezle_chart.setInteractive(true);
        greezle_chart.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
        greezle_chart.setContainerScrollEnabled(true, ContainerScrollType.HORIZONTAL);


        // tree_chart.setLineChartData(data);

        progressBarHandler.post(new Runnable() {
            public void run() {
                long treeCount = userService.getUserProfile().getTreeCount();
                int level = levelService.getLevel(treeCount);
                long treeToNextLevel = levelService.treesTillNextLevel(treeCount);

                int profileLevelProgress = 0;
                if (treeCount != 0) {
                    profileLevelProgress = (int) (100.0 / (treeToNextLevel)) ;
                }

                ProgressBar profileLevelBar = (ProgressBar) rootView.
                        findViewById(R.id.profileLevelBar);
                profileLevelBar.setProgress(profileLevelProgress);

                TextView profileLevel = (TextView) rootView.findViewById(R.id.profileLevelValue);
                profileLevel.setText(String.valueOf(level));

                double co2Percentage = co2Service.currentProgressInPercent(treeCount) * 100.0;

                ProgressBar profileCo2Bar = (ProgressBar) rootView.
                        findViewById(R.id.profileCo2LevelBar);
                profileCo2Bar.setProgress((int) (co2Percentage * 10));

                TextView profileCo2Level = (TextView) rootView.findViewById(R.id.profileCo2LevelValue);
                profileCo2Level.setText(String.format("%.1f%%", co2Percentage));
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ProfileCard profileCard = new ProfileCard(getContext(), userService);
        profileCard.setProfileCard(view);
    }
}