package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import de.hackathon_freiburg.sparks.greezthetreez.services.LevelService;

public class DefaultLevelService implements LevelService {
    @Override
    public int getLevel(long treeCount) {
        int level = 0;
        long a = 1;
        long b = 1;
        long c = 0;
        while (c < treeCount) {
            level++;
            c = a + b;
            a = b;
            b = c;
            System.out.println("c: " + c);
        }
        return level;
    }

    @Override
    public long treesTillNextLevel(long treeCount) {
        long treesTillNextLevel = 0;
        long a = 1;
        long b = 1;
        long c = 0;
        while (treesTillNextLevel <= 0) {
            c = a + b;
            a = b;
            b = c;
            treesTillNextLevel = c - treeCount + 1;
        }
        return treesTillNextLevel;
    }
}
