package de.hackathon_freiburg.sparks.greezthetreez.services.impl;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by bene on 20.05.17.
 */
public class DefaultLevelServiceTest {
    @Test
    public void getLevel() throws Exception {
        DefaultLevelService service = new DefaultLevelService();
        assertEquals(8, service.getLevel(35));
    }

    @Test
    public void treesTillNextLevel(){
        DefaultLevelService service = new DefaultLevelService();
        assertEquals(13, service.treesTillNextLevel(22));
    }

}